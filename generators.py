import random
import string


MIN_STR_LEN = 15
MAX_STR_LEN = 25
MAX_SPACES = 2

MIN_NUM = 1000
MAX_NUM = 90000000


def alphabetical():
    return ''.join(random.choice(string.ascii_lowercase) for i in range(random.randrange(MIN_STR_LEN, MAX_STR_LEN)))


def alphanumeric():
    choices = list(string.ascii_lowercase + string.digits)
    random.shuffle(choices)

    return ' ' * random.randrange(MAX_SPACES) + \
           ''.join(random.choice(choices) for i in range(random.randrange(MIN_STR_LEN, MAX_STR_LEN))) + \
           ' ' * random.randrange(MAX_SPACES)


def integer():
    return str(random.randrange(MIN_NUM, MAX_NUM))


def real_number():
    return str(round(random.uniform(MIN_NUM, MAX_NUM), random.randrange(1, 10)))
