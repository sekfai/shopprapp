import os
import random

import generators as g


MAX_FILE_SIZE = 1000
GENERATORS = [g.alphabetical, g.alphanumeric, g.integer, g.real_number]


def generate(filename):
    with open(filename, 'w') as f:
        count = 0

        while True:
            obj = random.choice(GENERATORS)()

            if count + len(obj) > MAX_FILE_SIZE:
                break

            f.write(obj)
            f.write(', ')

            count += len(obj)


if __name__ == '__main__':
    generate('output.txt')
