import os


def has_digit(string):
    return any(char.isdigit() for char in string)


def is_alphanumeric(s):
    return s.isalnum() and has_digit(s)


def is_alphabetical(s):
    return s.isalpha()


def is_integer(s):
    try:
        return str(int(s)) == s
    except ValueError:
        return False


def is_real_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def objects(f):
    temp = []
    while True:
        chunk = f.read(40)
        if chunk == '':
            break

        for c in chunk:
            if c == ',':
                s = ''.join(temp).strip()
                if s:
                    yield s
                temp = []
            else:
                temp.append(c)

    s = ''.join(temp).strip()
    if s:
        yield s


if __name__ == '__main__':
    with open('output.txt') as f:
        for o in objects(f):
            if is_integer(o):
                print('%s - integer' % o)
            elif is_real_number(o):
                print('%s - real numbers' % o)
            elif is_alphabetical(o):
                print('%s - alphabetical strings' % o)
            elif is_alphanumeric(o):
                print('%s - alphanumeric' % o)
